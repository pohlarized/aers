mod error;

use crate::error::AesError;

pub enum Aes<'a> {
    Aes128(&'a [u8; 16]),
    Aes196(&'a [u8; 24]),
    Aes256(&'a [u8; 32]),
}

impl<'a> Aes<'a> {
    pub fn encrypt(&self, block: &[u8; 16]) -> Result<[u8; 16], AesError> {
        match *self {
            Aes::Aes128(key) => aes_encrypt(block, key),
            Aes::Aes196(key) => aes_encrypt(block, key),
            Aes::Aes256(key) => aes_encrypt(block, key),
        }
    }
}

type State = [u8; 16];
const ROW_INDEXES: [u8; 16] = [0, 4, 8, 12, 1, 5, 9, 13, 2, 6, 10, 14, 3, 7, 11, 15];
const SHIFTED_ROW_INDEXES: [u8; 16] = [0, 4, 8, 12, 5, 9, 13, 1, 10, 14, 2, 6, 15, 3, 7, 11];
const S_BOX: [u8; 256] = [
    0x63, 0x7c, 0x77, 0x7b, 0xf2, 0x6b, 0x6f, 0xc5, 0x30, 0x01, 0x67, 0x2b, 0xfe, 0xd7, 0xab, 0x76,
    0xca, 0x82, 0xc9, 0x7d, 0xfa, 0x59, 0x47, 0xf0, 0xad, 0xd4, 0xa2, 0xaf, 0x9c, 0xa4, 0x72, 0xc0,
    0xb7, 0xfd, 0x93, 0x26, 0x36, 0x3f, 0xf7, 0xcc, 0x34, 0xa5, 0xe5, 0xf1, 0x71, 0xd8, 0x31, 0x15,
    0x04, 0xc7, 0x23, 0xc3, 0x18, 0x96, 0x05, 0x9a, 0x07, 0x12, 0x80, 0xe2, 0xeb, 0x27, 0xb2, 0x75,
    0x09, 0x83, 0x2c, 0x1a, 0x1b, 0x6e, 0x5a, 0xa0, 0x52, 0x3b, 0xd6, 0xb3, 0x29, 0xe3, 0x2f, 0x84,
    0x53, 0xd1, 0x00, 0xed, 0x20, 0xfc, 0xb1, 0x5b, 0x6a, 0xcb, 0xbe, 0x39, 0x4a, 0x4c, 0x58, 0xcf,
    0xd0, 0xef, 0xaa, 0xfb, 0x43, 0x4d, 0x33, 0x85, 0x45, 0xf9, 0x02, 0x7f, 0x50, 0x3c, 0x9f, 0xa8,
    0x51, 0xa3, 0x40, 0x8f, 0x92, 0x9d, 0x38, 0xf5, 0xbc, 0xb6, 0xda, 0x21, 0x10, 0xff, 0xf3, 0xd2,
    0xcd, 0x0c, 0x13, 0xec, 0x5f, 0x97, 0x44, 0x17, 0xc4, 0xa7, 0x7e, 0x3d, 0x64, 0x5d, 0x19, 0x73,
    0x60, 0x81, 0x4f, 0xdc, 0x22, 0x2a, 0x90, 0x88, 0x46, 0xee, 0xb8, 0x14, 0xde, 0x5e, 0x0b, 0xdb,
    0xe0, 0x32, 0x3a, 0x0a, 0x49, 0x06, 0x24, 0x5c, 0xc2, 0xd3, 0xac, 0x62, 0x91, 0x95, 0xe4, 0x79,
    0xe7, 0xc8, 0x37, 0x6d, 0x8d, 0xd5, 0x4e, 0xa9, 0x6c, 0x56, 0xf4, 0xea, 0x65, 0x7a, 0xae, 0x08,
    0xba, 0x78, 0x25, 0x2e, 0x1c, 0xa6, 0xb4, 0xc6, 0xe8, 0xdd, 0x74, 0x1f, 0x4b, 0xbd, 0x8b, 0x8a,
    0x70, 0x3e, 0xb5, 0x66, 0x48, 0x03, 0xf6, 0x0e, 0x61, 0x35, 0x57, 0xb9, 0x86, 0xc1, 0x1d, 0x9e,
    0xe1, 0xf8, 0x98, 0x11, 0x69, 0xd9, 0x8e, 0x94, 0x9b, 0x1e, 0x87, 0xe9, 0xce, 0x55, 0x28, 0xdf,
    0x8c, 0xa1, 0x89, 0x0d, 0xbf, 0xe6, 0x42, 0x68, 0x41, 0x99, 0x2d, 0x0f, 0xb0, 0x54, 0xbb, 0x16,
];

const RCON: [u8; 10] = [0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80, 0x1B, 0x36];

/// Fixed blocksize of 128 bits, but variable keylength of either 128, 192 or 256 bits.
fn aes_encrypt(block: &[u8; 16], key: &[u8]) -> Result<[u8; 16], AesError> {
    let mut state: State = *block;
    let key_schedule = key_expansion(key)?;

    add_round_key(&mut state, &key_schedule, 0);

    let key_words = key.len() / 4;
    let num_rounds = rounds_for_key_words(key_words)?;
    for round in 1..num_rounds {
        sub_bytes(&mut state);
        shift_rows(&mut state);
        mix_columns(&mut state);
        add_round_key(&mut state, &key_schedule, round);
    }
    sub_bytes(&mut state);
    shift_rows(&mut state);
    add_round_key(&mut state, &key_schedule, num_rounds);

    Ok(state)
}

fn sub_bytes(state: &mut State) {
    for i in 0..state.len() {
        state[i] = S_BOX[state[i] as usize];
    }
}

fn shift_rows(state: &mut State) {
    let mut new_cols = [0u8; 16];
    for i in 0..16 {
        new_cols[ROW_INDEXES[i] as usize] = state[SHIFTED_ROW_INDEXES[i] as usize]
    }
    *state = new_cols;
}

fn mix_column(column: &mut [u8]) {
    // Since we only call this from `mix_columns` through `chunks_exact_mut`, we know that the
    // chunks will always have size 4!
    let all_xor = column[0] ^ column[1] ^ column[2] ^ column[3];
    let old_c0 = column[0];
    column[0] = column[0] ^ xtime(column[0] ^ column[1]) ^ all_xor;
    column[1] = column[1] ^ xtime(column[1] ^ column[2]) ^ all_xor;
    column[2] = column[2] ^ xtime(column[2] ^ column[3]) ^ all_xor;
    column[3] = column[3] ^ xtime(column[3] ^ old_c0) ^ all_xor;
}

fn xtime(a: u8) -> u8 {
    if a & 0x80 > 0 {
        (a << 1) ^ 0x1b
    } else {
        a << 1
    }
}

fn mix_columns(state: &mut State) {
    for column in state.chunks_exact_mut(4) {
        mix_column(column);
    }
}

fn add_round_key(state: &mut State, key_schedule: &[[u8; 4]], round: u8) {
    // We are guaranteed that key_schedule.len() >= 4*round by our calculations in the key_expansion
    // function.
    let round_keys: [[u8; 4]; 4] = [
        key_schedule[(4 * round) as usize],
        key_schedule[(4 * round + 1) as usize],
        key_schedule[(4 * round + 2) as usize],
        key_schedule[(4 * round + 3) as usize],
    ];
    for (column_i, column) in state.chunks_exact_mut(4).enumerate() {
        for (elem_i, elem) in column.iter_mut().enumerate() {
            *elem ^= round_keys[column_i][elem_i]
        }
    }
}

fn rounds_for_key_words(key_words: usize) -> Result<u8, AesError> {
    match key_words {
        4 => Ok(10),
        6 => Ok(12),
        8 => Ok(14),
        other => Err(AesError::InvalidKeySize(other)),
    }
}

fn arr_xor(arr1: &[u8; 4], arr2: &[u8; 4]) -> [u8; 4] {
    [
        arr1[0] ^ arr2[0],
        arr1[1] ^ arr2[1],
        arr1[2] ^ arr2[2],
        arr1[3] ^ arr2[3],
    ]
}

fn key_expansion(key: &[u8]) -> Result<Vec<[u8; 4]>, AesError> {
    let num_key_words = key.len() / 4; // AES-Spec: Nk
    let num_state_words = 4; // AES-Spec: Nb
    let num_rounds = rounds_for_key_words(num_key_words)?;

    // initialize state w
    // depending on the keysize w is 4*4, 4*6 or 4*8 words long
    println!("Key length: {}; Key: {:?}", key.len(), key);
    let mut w: Vec<[u8; 4]> = vec![[0; 4]; num_state_words as usize * (num_rounds as usize + 1)];
    for i in 0..num_key_words {
        w[i] = [key[4 * i], key[4 * i + 1], key[4 * i + 2], key[4 * i + 3]];
    }

    for i in num_key_words..(num_state_words as usize * (num_rounds as usize + 1)) {
        let mut tmp = w[i - 1];
        if i % num_key_words == 0 {
            tmp = arr_xor(&sub_word(&rot_word(&tmp)), &rcon(i / num_key_words));
        } else if num_key_words > 6 && i % num_key_words == 4 {
            tmp = sub_word(&tmp);
        }
        w[i] = arr_xor(&w[i - num_key_words], &tmp);
    }
    Ok(w)
}

fn sub_word(input: &[u8; 4]) -> [u8; 4] {
    input.map(|x| S_BOX[x as usize])
}

fn rot_word(input: &[u8; 4]) -> [u8; 4] {
    [input[1], input[2], input[3], input[0]]
}

fn rcon(index: usize) -> [u8; 4] {
    // Should be fine, `index` should be guaranteed to be between 1 and 10 (inclusive).
    [RCON[index - 1], 0, 0, 0]
}

#[cfg(test)]
mod tests {
    use crate::Aes;
    use std::{fmt, num::ParseIntError};

    #[derive(Debug, Clone, PartialEq, Eq)]
    pub enum DecodeHexError {
        OddLength,
        ParseInt(ParseIntError),
    }

    impl From<ParseIntError> for DecodeHexError {
        fn from(e: ParseIntError) -> Self {
            DecodeHexError::ParseInt(e)
        }
    }

    impl fmt::Display for DecodeHexError {
        fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
            match self {
                DecodeHexError::OddLength => "input string has an odd number of bytes".fmt(f),
                DecodeHexError::ParseInt(e) => e.fmt(f),
            }
        }
    }

    impl std::error::Error for DecodeHexError {}

    // from https://stackoverflow.com/a/52992629
    pub fn hex_to_bytes(s: &str) -> Result<Vec<u8>, DecodeHexError> {
        if s.len() % 2 != 0 {
            Err(DecodeHexError::OddLength)
        } else {
            (0..s.len())
                .step_by(2)
                .map(|i| u8::from_str_radix(&s[i..i + 2], 16).map_err(|e| e.into()))
                .collect()
        }
    }

    // from https://stackoverflow.com/a/29784723
    pub fn vec_to_arr_16(vec: Vec<u8>) -> [u8; 16] {
        let mut arr = [0u8; 16];
        for (place, element) in arr.iter_mut().zip(vec.iter()) {
            *place = *element;
        }
        arr
    }

    pub fn vec_to_arr_24(vec: Vec<u8>) -> [u8; 24] {
        let mut arr = [0u8; 24];
        for (place, element) in arr.iter_mut().zip(vec.iter()) {
            *place = *element;
        }
        arr
    }

    pub fn vec_to_arr_32(vec: Vec<u8>) -> [u8; 32] {
        let mut arr = [0u8; 32];
        for (place, element) in arr.iter_mut().zip(vec.iter()) {
            *place = *element;
        }
        arr
    }

    #[test]
    fn aes_128() {
        let plaintext = vec_to_arr_16(hex_to_bytes("00112233445566778899aabbccddeeff").unwrap());
        let key = vec_to_arr_16(hex_to_bytes("000102030405060708090a0b0c0d0e0f").unwrap());
        let expected_result =
            vec_to_arr_16(hex_to_bytes("69c4e0d86a7b0430d8cdb78070b4c55a").unwrap());
        let aes = Aes::Aes128(&key);
        let res = aes.encrypt(&plaintext);
        assert!(res.is_ok());
        assert_eq!(res.as_ref().unwrap(), &expected_result);
    }

    #[test]
    fn aes_196() {
        let plaintext = vec_to_arr_16(hex_to_bytes("00112233445566778899aabbccddeeff").unwrap());
        let key = vec_to_arr_24(
            hex_to_bytes("000102030405060708090a0b0c0d0e0f1011121314151617").unwrap(),
        );
        let expected_result =
            vec_to_arr_16(hex_to_bytes("dda97ca4864cdfe06eaf70a0ec0d7191").unwrap());
        let aes = Aes::Aes196(&key);
        let res = aes.encrypt(&plaintext);
        assert!(res.is_ok());
        assert_eq!(res.as_ref().unwrap(), &expected_result);
    }

    #[test]
    fn aes_256() {
        let plaintext = vec_to_arr_16(hex_to_bytes("00112233445566778899aabbccddeeff").unwrap());
        let key = vec_to_arr_32(
            hex_to_bytes("000102030405060708090a0b0c0d0e0f101112131415161718191a1b1c1d1e1f")
                .unwrap(),
        );
        let expected_result =
            vec_to_arr_16(hex_to_bytes("8ea2b7ca516745bfeafc49904b496089").unwrap());
        let aes = Aes::Aes256(&key);
        let res = aes.encrypt(&plaintext);
        assert!(res.is_ok());
        assert_eq!(res.as_ref().unwrap(), &expected_result);
    }
}
