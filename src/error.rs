use std::fmt;

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum AesError {
    InvalidKeySize(usize),
}

impl fmt::Display for AesError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            AesError::InvalidKeySize(size) => {
                format!("Key size {} is not one of 128, 192 or 256.", size).fmt(f)
            }
        }
    }
}
